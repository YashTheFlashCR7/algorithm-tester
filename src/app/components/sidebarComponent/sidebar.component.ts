/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import * as $ from "jquery";

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/layout/dashboard', title: 'Array Dashboard',  icon: 'dashboard', class: '' },
];


/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-sidebar',
    templateUrl: './sidebar.template.html'
})

export class sidebarComponent implements OnInit {
    menuItems: any[];

    constructor() { }
  
    ngOnInit() {
      this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

}
