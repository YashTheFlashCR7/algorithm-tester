/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import * as Chartist from 'chartist';

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-arraydashboard',
    templateUrl: './arraydashboard.template.html'
})

export class arraydashboardComponent implements OnInit {

    algorithmArray = [
        {
            Id: 'MN',
            AGIndex: 1,
            Title: 'How do you find the missing number in a given integer array of 1 to 100?',
            Subtitle: 'An implementation of array processing',
            ArrayDisplay: ['Array = {1, 2, 4,, 6, 3, 7, 8}'],
            ArrayQuestion: ['Given the following array , find the missing number:'],
            ArrayAnswer: [''],
            ArrayValues: [
                {
                    Array: [1, 2, 4, 6, 3, 7, 8]
                },
            ]
        },
        {
            Id: 'RN',
            AGIndex: 2,
            Title: 'Find the two repeating elements in a given array',
            Subtitle: 'An implementation of array processing',
            ArrayDisplay: ['Array = {4, 2, 4, 5, 2, 3, 1} and n = 5'],
            ArrayQuestion: [
                `You are given an array of n+2 elements. All elements of the array are in range 1 to n. 
                And all elements occur once except two numbers which occur twice.`,
                'Find the two repeating numbers?'],
            ArrayAnswer: [''],
            ArrayValues: [
                {
                    Array: [4, 2, 4, 5, 2, 3, 1]
                },
            ]
        },
        {
            Id: 'LNSM',
            AGIndex: 3,
            Title: 'Find the largest and smallest number in an unsorted integer array?',
            Subtitle: 'An implementation of array processing',
            ArrayDisplay: ['Array = {4, 2, 7, 5, 8, 3, 1}'],
            ArrayQuestion: [
                `You are given an un-ordered array of integers`,
                'Find the largest and smallest value in this array'],
            ArrayAnswer: [''],
            ArrayValues: [
                {
                    Array: [4, 2, 7, 5, 8, 3, 1]
                },
            ]
        },
        {
            Id: 'QSA',
            AGIndex: 4,
            Title: 'How is an integer array sorted in place using the quicksort algorithm?',
            Subtitle: 'An implementation of array processing',
            ArrayDisplay: ['Arr = {10, 7, 8, 9, 1, 5}'],
            ArrayQuestion: [
                `You are given an un-ordered array of integers`,
                'Sort this array using the quicksort method'],
            ArrayAnswer: [''],
            ArrayValues: [
                {
                    Array: [10, 7, 8, 9, 1, 5]
                },
            ]
        },

        {
            Id: 'RDA',
            AGIndex: 5,
            Title: 'Remove duplicates from sorted array',
            Subtitle: 'An implementation of array processing',
            ArrayDisplay: ['Arr = {1, 2, 2, 3, 4, 4, 4, 5, 5}'],
            ArrayQuestion: [
                `Given a sorted array, the task is to remove the duplicate elements from the array.`],
            ArrayAnswer: [''],
            ArrayValues: [
                {
                    Array: [1, 2, 2, 3, 4, 4, 4, 5, 5]
                },
            ]
        },
        {
            Id: 'RAP',
            AGIndex: 6,
            Title: 'Reversing an array In place',
            Subtitle: 'An implementation of array processing',
            ArrayDisplay: ['Arr = {1, 2, 2, 3, 4, 4, 4, 5, 5}'],
            ArrayQuestion: [
                `Given the following array , please reverse it and display it`],
            ArrayAnswer: [''],
            ArrayValues: [
                {
                    Array: [1, 2, 2, 3, 4, 4, 4, 5, 5]
                },
            ]
        },
    ]


    constructor() { }

    ngOnInit() {
    }
    /**=======================================================================================================================
     * ALGORITHMS
     =========================================================================================================================*/
    findTheMissingNumberAlgorithm(a: number[], n: number) {
        let total: number = (n + 1) * (n + 2) / 2 // get total
        for (let i = 0; i < n; i++) {
            total = total - a[i]
        }
        return total
    }

    findTherepeatingNumberAlgorithm(a: number[], n: number, index: number) {
        for (let firstIndex = 0; firstIndex < n; firstIndex++) {
            for (let secondIndex = firstIndex + 1; secondIndex < n; secondIndex++) {
                if (a[firstIndex] === a[secondIndex]) {
                    this.addToAnswers(index, a[firstIndex].toString())
                }
            }
        }
    }

    findTheLargestSmallestNumberAlgorithm(a: number[], index: number, context: string) {
        let reference: number = a[0]
        for (let i = 0; i < a['length']; i++) {
            if (context === 'min') {
                if (a[i] < reference) {
                    reference = a[i]
                }
            }

            else {
                if (a[i] > reference) {
                    reference = a[i]
                }
            }
        }

        this.addToAnswers(index, context + ":" + reference.toString())

    }

    /* This function takes last element as pivot, places the pivot element at its correct position in sorted array, and places all 
    smaller (smaller than pivot) to left of  pivot and all greater elements to right of pivot */
    quicksort_partition(arr: number[], low: number, high: number) {
        let pivot: number = arr[high];

        // index of smaller element 
        let i: number = (low - 1);
        for (let j = low; j < high; j++) {
            // If current element is smaller  
            // than the pivot 
            if (arr[j] < pivot) {
                i++;

                // swap arr[i] and arr[j] 
                let temp: number = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        // swap arr[i+1] and arr[high] (or pivot) 
        let temp1: number = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp1;

        return i + 1;
    }

    /* The main function that implements QuickSort() arr[] --> Array to be sorted, low --> Starting index,  high --> Ending index */
    quicksort_call(a: number[], low: number, high: number, index: number) {
        if (low < high) {
            /* pi is partitioning index, arr[pi] is  now at right place */
            let partionIndex: number = this.quicksort_partition(a, low, high)
            // Recursively sort elements before partition and after partition
            this.quicksort_partition(a, low, partionIndex - 1)
            this.quicksort_partition(a, partionIndex + 1, high)
        }

        this.addToAnswers(index, this.quicksort_print(a, a.length))
    }

    quicksort_print(array: number[], length: number) {
        let Arraystring: string = "{"
        for (let i = 0; i < length; ++i) {
            Arraystring = Arraystring + array[i] + ", ";
        }
        return Arraystring + "}"

    }

    removeDuplicates(arr: number[], n: number, index: number) {
        debugger
        let temp: number[] = []
        let tempCounter: number = 0
        for (let i = 0; i < n - 1; i++) {
            // If current element is not equal to next element then store that current element
            if (arr[i] != arr[i + 1]) {
                temp[tempCounter] = arr[i]
                tempCounter++
            }
        }

        // Store the last element as whether it is unique or repeated, it hasn't stored previously 

        temp[tempCounter++] = arr[n - 1]

        for (let i = 0; i < tempCounter; i++) {
            debugger
            arr[i] = temp[i]
        }

        this.addToAnswers(index, arr.toString())
    }


    reverseArray(arr:number[],n:number , index:number){
        let arrIndex = n -1
        let tempIndex = 0
        let temp:number 
        while(arrIndex>tempIndex){
            temp = arr[arrIndex]
            arr[arrIndex] = arr[tempIndex]
            arr[tempIndex] = temp
            arrIndex--
            tempIndex++
        }

        this.addToAnswers(index , arr.toString())

    }


    /**=======================================================================================================================
     * CALL METHODS
     =========================================================================================================================*/
    callMissingNumberMethod(index: number) {
        this.algorithmArray[index - 1]['ArrayAnswer'].push(
            this.findTheMissingNumberAlgorithm(this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array'],
                this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array']['length']).toString()
        )
    }

    callrepeatingNumberMethod(index: number) {
        this.findTherepeatingNumberAlgorithm(this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array'],
            this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array']['length'], index)
    }

    callFindTheLargestSmallestNumberMethod(index: number, context: string) {
        this.findTheLargestSmallestNumberAlgorithm(this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array'], index, context)
    }

    callQuicksortAlgorithm(index: number) {
        this.quicksort_call(this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array'], 0,
            this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array']['length'] - 1, index)
    }

    callRemoveDuplicatesAlgorithm(index: number) {
        this.removeDuplicates(this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array'],
            this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array']['length'], index)
    }

    callReverseArrayInPlaceAlgorithm(index:number){
        this.reverseArray(this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array'],
        this.algorithmArray[index - 1]['ArrayValues'][index - index]['Array']['length'], index)
    }

    /**=======================================================================================================================
   * COMMON METHODS
   =========================================================================================================================*/
    addToAnswers(index: number, answer: string) {
        this.algorithmArray[index - 1]['ArrayAnswer'].push(answer)
    }

    coordinateAlgorithms(key: string, index: number) {
        switch (key) {
            case 'MN':
                this.callMissingNumberMethod(index)
                break;

            case 'RN':
                this.callrepeatingNumberMethod(index)
                break;

            case 'LNSM':
                this.callFindTheLargestSmallestNumberMethod(index, 'max')
                this.callFindTheLargestSmallestNumberMethod(index, 'min')
                break;

            case 'QSA':
                this.callQuicksortAlgorithm(index)
                break;

            case 'RDA':
                this.callRemoveDuplicatesAlgorithm(index)
                break;

                case 'RAP':
                this.callReverseArrayInPlaceAlgorithm(index)
                break;


        }
    }


}
