export const environment = {
    "envId": "76093f7f-b2fe-a255-cb80-3e4d372e625a",
    "name": "dev",
    "properties": {
        "production": false,
        "baseUrl": "http://localhost:3000/bhive-art/",
        "tenantName": "telesure",
        "appName": "Algorithmtester",
        "namespace": "com.telesure.Algorithmtester",
        "isNotificationEnabled": false,
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "appDataSource": "telesure-rt",
        "appAuthenticationStrategy": "basicAuth",
        "basicAuthUser": "username",
        "basicAuthPassword": "password",
        "useDefaultExceptionUI": true
    }
}